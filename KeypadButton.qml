import QtQuick 2.0

Rectangle {
    property alias text: description.text
    property alias textColor: description.color
    property var pixelSize: description.font.pixelSize
    property var action
    property var releaseAction

    width: 100
    height: 100
    color: "grey"

    Text {
        id: description
        anchors.centerIn: parent
        color: "black"
        font.pixelSize: text.length == 1 ? parent.height * 0.7 : parent.height * 0.25
    }

    MouseArea {
        anchors.fill: parent
        onPressed: {
            action(parent.text)
        }

        onReleased: {
            if(releaseAction) {
                releaseAction();
            }
        }
    }
}
