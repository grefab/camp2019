import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.VirtualKeyboard 2.4
import QtGraphicalEffects 1.12
import wtf.camp2019 1.0
import "colorHelper.js" as ColorHelper

Window {
    id: window
    visible: true
    visibility: Window.FullScreen
    width: 640
    height: 480
    title: qsTr("Mr Bank")
    property color keyColor: "grey"

    DataAccess {
        id: dataAccess
    }

    Timer {
        repeat: true
        running: true
        interval: 10

        property int idx: 0
        onTriggered: {
            idx += 1
            background1.x = idx
            while(background1.x > parent.width) {
                background1.x -= parent.width;
            }

            background2.x = background1.x - background2.width;
        }
    }

    LinearGradient {
        id: background1
        x: 0
        y: 0
        width: parent.width
        height: parent.height
        start: Qt.point(0, 0)
        end: Qt.point(parent.width, 0)
        gradient: Gradient {
            GradientStop {
                position: 0.000
                color: Qt.rgba(1, 0, 0, 1)
            }
            GradientStop {
                position: 0.167
                color: Qt.rgba(1, 1, 0, 1)
            }
            GradientStop {
                position: 0.333
                color: Qt.rgba(0, 1, 0, 1)
            }
            GradientStop {
                position: 0.500
                color: Qt.rgba(0, 1, 1, 1)
            }
            GradientStop {
                position: 0.667
                color: Qt.rgba(0, 0, 1, 1)
            }
            GradientStop {
                position: 0.833
                color: Qt.rgba(1, 0, 1, 1)
            }
            GradientStop {
                position: 1.000
                color: Qt.rgba(1, 0, 0, 1)
            }
        }
    }

    LinearGradient {
        id: background2
        x: parent.width
        y: 0
        width: parent.width
        height: parent.height
        start: Qt.point(0, 0)
        end: Qt.point(parent.width, 0)
        gradient: Gradient {
            GradientStop {
                position: 0.000
                color: Qt.rgba(1, 0, 0, 1)
            }
            GradientStop {
                position: 0.167
                color: Qt.rgba(1, 1, 0, 1)
            }
            GradientStop {
                position: 0.333
                color: Qt.rgba(0, 1, 0, 1)
            }
            GradientStop {
                position: 0.500
                color: Qt.rgba(0, 1, 1, 1)
            }
            GradientStop {
                position: 0.667
                color: Qt.rgba(0, 0, 1, 1)
            }
            GradientStop {
                position: 0.833
                color: Qt.rgba(1, 0, 1, 1)
            }
            GradientStop {
                position: 1.000
                color: Qt.rgba(1, 0, 0, 1)
            }
        }
    }

    function onKeyPressed(key) {
        hypnotoad.visible = true;
        if(pinDisplay.text.length < 4) {
            pinDisplay.text += key;
        }
    }

    function onKeyReleased() {
        hypnotoad.visible = false;
    }

    function onClrPressed(key) {
        pinDisplay.text = "";
        onOkPressed(key);
    }

    function onOkPressed(key) {
        if(pinDisplay.text.length > 0) {
            instructionDisplay.text = "THANKS FOR YOUR\nDONATION.";
            suatmm.visible = true;
            resetTimer.start();
            dataAccess.store(pinDisplay.text);
        }
    }

    Timer {
        id: resetTimer
        interval: 3000
        repeat: false
        running: false
        onTriggered: {
            pinDisplay.text = ""
            instructionDisplay.text = "PLEASE ENTER PIN"
            suatmm.visible = false;
        }
    }

    Row {
        anchors.fill: parent
        Item {
            width: parent.width / 2
            height: parent.height

            VideoDisplay {
                id: videoDisplay
                anchors.fill: parent
            }

            Image {
                id: hypnotoad
                anchors.fill: parent
                visible: false
                source: "qrc:/hypnotoad.jpg"
                fillMode: Image.PreserveAspectFit
            }

            Image {
                id: suatmm
                anchors.fill: parent
                visible: false
                //        source: "qrc:/takemymoney.jpg"
                source: "qrc:/1757072096-Zoidberg_-_hooray_ive_accomplished_nothing.jpg"
                fillMode: Image.PreserveAspectFit
            }
        }
        Item {
            width: parent.width / 2
            height: parent.height
            Column {
                id: keypadColumn
                anchors.centerIn: parent
                spacing: 4
                width: keypad.width

                Display {
                    id: instructionDisplay
                    text: "PLEASE ENTER PIN"
                    textColor: ColorHelper.complementaryColor(window.color)
                    font.pixelSize: 30

                    Timer {
                        running: true
                        repeat: true
                        interval: 100
                        property bool foo: false
                        onTriggered: {
                            instructionDisplay.textColor = foo ? "black" : "white";
                            pinDisplay.textColor = !foo ? "black" : "white";
                            foo = !foo;
                        }
                    }
                }

                Display {
                    id: pinDisplay
                    textColor: ColorHelper.complementaryColor(window.color)
                }

                Grid {
                    id: keypad
                    columns: 4
                    rows: 4
                    spacing: 4
                    KeypadButton { text: "1";   action: function(text){ window.onKeyPressed(text); }; releaseAction: window.onKeyReleased; color: keyColor; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "2";   action: function(text){ window.onKeyPressed(text); }; releaseAction: window.onKeyReleased; color: keyColor; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "3";   action: function(text){ window.onKeyPressed(text); }; releaseAction: window.onKeyReleased; color: keyColor; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "CLR"; action: function(text){ window.onClrPressed(text); }; releaseAction: window.onKeyReleased; color: "white"; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "4";   action: function(text){ window.onKeyPressed(text); }; releaseAction: window.onKeyReleased; color: keyColor; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "5";   action: function(text){ window.onKeyPressed(text); }; releaseAction: window.onKeyReleased; color: keyColor; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "6";   action: function(text){ window.onKeyPressed(text); }; releaseAction: window.onKeyReleased; color: keyColor; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "ABRT"; action: function(text){ window.onClrPressed(text); }; releaseAction: window.onKeyReleased; color: "red"; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "7";   action: function(text){ window.onKeyPressed(text); }; releaseAction: window.onKeyReleased; color: keyColor; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "8";   action: function(text){ window.onKeyPressed(text); }; releaseAction: window.onKeyReleased; color: keyColor; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "9";   action: function(text){ window.onKeyPressed(text); }; releaseAction: window.onKeyReleased; color: keyColor; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "CORR"; action: function(text){ window.onClrPressed(text); }; releaseAction: window.onKeyReleased; color: "yellow"; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "*"; textColor: ColorHelper.complementaryColor(keyColor); releaseAction: window.onKeyReleased }
                    KeypadButton { text: "0"; action: function(text){ window.onKeyPressed(text); }; releaseAction: window.onKeyReleased; color: keyColor; textColor: ColorHelper.complementaryColor(keyColor) }
                    KeypadButton { text: "#"; textColor: ColorHelper.complementaryColor(keyColor); releaseAction: window.onKeyReleased }
                    KeypadButton { text: "OK"; action: function(text){ window.onOkPressed(text); }; releaseAction: window.onKeyReleased; color: "lime"; textColor: ColorHelper.complementaryColor(keyColor) }
                }
            }
        }
    }

    Rainbow {
        id: rainbow
        interval: 1000
        setColor: function(color) {
            window.color = color;
        }
    }

    Behavior on color {
        ColorAnimation {
            duration: rainbow.interval
        }
    }

    onColorChanged: {
        keyColor = ColorHelper.complementaryColor(color);
    }

    Text {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 10
        color: ColorHelper.complementaryColor(window.color)
        text: "FOR YOUR OWN SAFETY THIS\nTERMINAL IS CAMERA MONITORED"
        font.pixelSize: 30
    }

    Text {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 10
        color: ColorHelper.complementaryColor(window.color)
        text: "https://gitlab.com/grefab/camp2019"
    }

    Image {
        id: nyancat
        width: 288
        height: 180
        visible: true
        source: "qrc:/nyan_cat.png"
        fillMode: Image.PreserveAspectFit

        Behavior on x { NumberAnimation{ duration: nyantimer.interval } }
        Behavior on y { NumberAnimation{ duration: nyantimer.interval } }

        Timer {
            id: nyantimer
            repeat: true
            running: true
            interval: 500

            onTriggered: {
                nyancat.x = Math.random() * (window.width - nyancat.width);
                nyancat.y = Math.random() * (window.height - nyancat.height);
            }
        }
    }
}
