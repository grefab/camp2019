import QtQuick 2.0

/*
    .red {background:#ff3366;}
    .orange {background:#ff6633;}
    .yellow {background:#FFCC33;}
    .green {background:#33FF66;}
    .cyan {background:#33FFCC;}
    .sky {background:#33CCFF;}
    .blue {background:#3366FF;}
    .indigo {background:#6633FF;}
    .violet {background:#CC33FF;}
    .grey {background: #efefef;}
 */

Timer {
    property var setColor
    property var setComplementaryColor
    property var colors: [
        "#ff3366",
        "#ff6633",
        "#FFCC33",
        "#33FF66",
        "#33FFCC",
        "#33CCFF",
        "#3366FF",
        "#6633FF",
        "#CC33FF",
        "#efefef"
    ]
    property int idx: 0

    repeat: true
    running: true
    onTriggered: {
        idx += 1;
        idx %= colors.length;
        if(setColor) {
            setColor(colors[idx]);
        }
        if(setComplementaryColor) {
            setComplementaryColor(colors[idx]);
        }
    }
}
