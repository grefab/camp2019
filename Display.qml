import QtQuick 2.0

Rectangle {
    property alias text: displayText.text
    property alias textColor: displayText.color
    property alias font: displayText.font

    width: parent.width
    height: 100
    color: "white"
    border.width: 5
    border.color: "grey"

    Text {
        id: displayText
        anchors.centerIn: parent
        font.pixelSize: parent.height * 0.7
        text: ""
    }
}
