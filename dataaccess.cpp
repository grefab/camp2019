#include "dataaccess.h"
#include <QFile>
#include <QDateTime>
#include <QDebug>

DataAccess::DataAccess(QObject *parent) : QObject(parent)
{

}

void DataAccess::store(QString data)
{
    QFile file("mr_bank.txt");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
        qDebug() << "could not open file!";
        return;
    }

    QString s = QDateTime::currentDateTimeUtc().toString(Qt::ISODateWithMs);
    s += ";";
    s += "\"" + data + "\"";
    s += + "\n";
    if(file.write(s.toUtf8()) < 0) {
        qDebug() << "could not write to file!";
    }
}
