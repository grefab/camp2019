#ifndef DATAACCESS_H
#define DATAACCESS_H

#include <QObject>

class DataAccess : public QObject
{
    Q_OBJECT
public:
    explicit DataAccess(QObject *parent = nullptr);

public slots:
    void store(QString data);

};

#endif // DATAACCESS_H
